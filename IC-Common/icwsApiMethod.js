var EventSource = require('eventsource');
var httpRequestHelper = require('./httpRequestHelper');
var queueSubscriptionHelper = require('./queueSubscriptionHelper');
let backupDataUtil = require('../UTILS/recentQueueDetFileOps');
const logger = require('../UTILS/trace')('IC-Common.icwsApiMethod');

var icwsApiMethod = (function(exports) {
    // This provides an abstraction around the ICWS API call (connection, handle-notification, etc) and message processing mechanisms.
    //

    // Stores the current version of messaging supported by the connected ICWS session.
    // This value is used in helping to determine if short-polling or server sent events should be used for message processing.
    var icwsCurrentMessagingVersion = null;

    // EventSource object for when Server Sent Events is used.
    var eventSource = null;

    // This holds the value of the messaging version that supports server sent events.
    var icwsMessagingVersionForServerSentEvents = 2;

    // This holds the list of last attempted alternative switching servers.
    var icwsLastAttemptedSwitchServers = [];

    // This holds the list of available alternative switching servers.
    var icwsAvailableSwitchServers = [];

    // Workgroup Queue Type as defined in ICWS document, 1 = Custom, 5 = ACD
    let allowedQueueTypes = new Set([1, 5]);

    exports.isConnected = function() {
        return httpRequestHelper.isConnected();
    };

    exports.writeWorkgroupsToFile = async function() {
        let workgroupList = queueSubscriptionHelper.getSubscribedWorkgroups();
        if (workgroupList.length === 0) { return; }
        await backupDataUtil.writeSfDataToFile('workgroups', workgroupList);
    };

    //
    // ICWS connection
    //

    var sessionDisconnectCallback;

    /**
     * The callback for receiving the result from {@link icwsConnect}.
     * @callback connectCallback
     * @param {Boolean} success Indicates whether the connection attempt was successful.
     * @param {Object} result If successful, an object containing ICWS session details; otherwise, an object containing error information.
     *   @param {String} result.icwsCsrfToken If successful, the ICWS CSRF token.
     *   @param {String} result.icwsSessionId If successful, the ICWS session ID.
     *   @param {Number} result.status If not successful, the http status code for the connection failure.
     *   @param {String} result.responseText If not successful, a description of the error, if available.
     * @see icwsApiMethod.icwsConnect
     */

    /**
     * The callback for receiving disconnect events from {@link icwsConnect}.
     * @callback disconnectCallback
     * @param {String} reason The reason for the disconnect.
     * @see icwsApiMethod.icwsConnect
     */

    /**
     * Attempts to create an ICWS session.
     * @param {String} application The name of the application.  This is also displayed in ININ supervisory views.
     * @param {String} server The server name where ICWS is available.
     * @param {String} user The IC user name with which to connect.
     * @param {String} password The IC password name with which to connect.
     * @param {connectCallback} connectCallback The callback to invoke with the connection result.
     * @param {disconnectCallback} [optDisconnectCallback] For successful connections, the callback to invoke if the session is disconnected.
     * @throws {Error} The connectCallback was undefined.
     */
    exports.icwsConnect = function(application, server, user, password, connectCallback, optDisconnectCallback) {
        // Reconnect interval for establish a connection with alternate swith over server.
        var ICWS_RECONNECT_INTERVAL_MS = 15000;
        var payload, icwsCsrfTokenValue, icwsSessionIdValue;

        if (connectCallback === undefined) {
            throw new Error('Invalid argument "connectCallback".');
        }
        // Clear list of last attempted alternative servers.
        while (icwsLastAttemptedSwitchServers.length > 0) {
            icwsLastAttemptedSwitchServers.pop();
        }
        // Clear list of available alternative servers.
        while (icwsAvailableSwitchServers.length > 0) {
            icwsAvailableSwitchServers.pop();
        }
        // Cache the disconnect callback for later use.
        sessionDisconnectCallback = optDisconnectCallback;

        payload = {
            __type: 'urn:inin.com:connection:icAuthConnectionRequestSettings',
            applicationName: application,
            userID: user,
            password: password
        };
        function reconnectToHost(hostName) {
            // Check if connected is established and the base host name match previous tried alternate host names
            if (!exports.isConnected()) {
                if (icwsLastAttemptedSwitchServers.indexOf(hostName) === -1) {
                    icwsLastAttemptedSwitchServers.push(hostName);
                }
                // Perform ICWS connection attempt to alternate server.
                httpRequestHelper.sendSessionlessRequest(hostName, 'POST', '/connection', payload, sendSessionlessRequestCallback);
            }
        }

        function process503StatusResponse(status, jsonResponse) {
            logger.info('Inside process503StatusResponse');
            if (jsonResponse.alternateHostList) {
                logger.info('number of alternate host = ' + jsonResponse.alternateHostList.length);
                for (var i = jsonResponse.alternateHostList.length - 1; i >= 0; i--) {
                    var alternateHost = jsonResponse.alternateHostList[i];

                    if (icwsAvailableSwitchServers.indexOf(alternateHost) === -1) {
                        icwsAvailableSwitchServers.push(alternateHost);
                    }
                }
            }
            for (var n = icwsAvailableSwitchServers.length - 1; n >= 0; n--) {
                var availableHost = jsonResponse.alternateHostList[n];

                logger.info('Alternate Host = ' + availableHost);
                setTimeout(reconnectToHost.bind(this, availableHost), ICWS_RECONNECT_INTERVAL_MS);
            }
        }

        // Call back method
        async function sendSessionlessRequestCallback(status, jsonResponse, serverName) {
            logger.info('Inside sendSessionlessRequestCallback, Response status = ' + status);
            if (httpRequestHelper.isSuccessStatus(status)) {
                icwsCsrfTokenValue = jsonResponse.csrfToken;
                icwsSessionIdValue = jsonResponse.sessionId;

                // Cache the supported messaging version for this ICWS session connection.
                // This is used to help determine if we can use server sent events over short-polling for message processing.
                // The features property is an array that does not guarantee index positions of features,
                //   so we need to search it for the featureId we are interested in.
                if (jsonResponse.features) {
                    for (var i = jsonResponse.features.length - 1; i >= 0; i--) {
                        var featureObject = jsonResponse.features[i];

                        if (featureObject.featureId === 'messaging') {
                            icwsCurrentMessagingVersion = featureObject.version;
                            break;
                        }
                    }
                }

                logger.info('Current messaging version = ' + icwsCurrentMessagingVersion);

                // Cache the ICWS session connection information for use with future API requests.
                httpRequestHelper.setCredentials(serverName, user, icwsCsrfTokenValue, icwsSessionIdValue);

                // Start monitoring for connection state changed messages.
                initializeMessageCallbacks();
                startMessageProcessing();
                
                var workgroupList = await backupDataUtil.getRecentWorkgroups();
                logger.info(`Workgroup Lists ${workgroupList.length}`);
                // Subscribe to all the queues before connection got down.
                queueSubscriptionHelper.subscribeWorkgroupQueues(workgroupList);
                
                // Signal completion of the operation, with successful results.
                connectCallback(true,
                                {
                                    icwsCsrfToken: icwsCsrfTokenValue,
                                    icwsSessionId: icwsSessionIdValue
                                });
            } else if (status === 503) {
                // Handle 503 failures and connect to switch over server.
                process503StatusResponse(status, jsonResponse);
            } else {
                // Signal completion of the operation, with error details.
                connectCallback(false,
                                {
                                    status: status,
                                    responseText: jsonResponse
                                });
            }
        }

        logger.info('Connect to ICWS, Establish a connection to the ICWS server. servername = ' + server);

        // Adding the "features" value for the optional "include" query string so we can retrieve the currently supported messaging version.
        httpRequestHelper.sendSessionlessRequest(server, 'POST', '/connection?include=features', payload, sendSessionlessRequestCallback);
    };

    /**
     * Attempts to call handler using handle notification ICWS API
     * @param {String} xmlString Email interaction data(interaction attributes) in XML string format.
     */
    exports.icwsHandleNotification = function(xmlString, workgroupName, itemId) {
        return new Promise((resolve, reject) => {
            canWorkgroupQueueEmails(workgroupName).then(() => {
                // Do nothing: proceed further
            }).catch((err) => {
                return reject(err);
            });

            queueSubscriptionHelper.isObjectInProcessing(itemId).then(() => {
                return reject(new Error(`Object already in processing with item Id ${itemId}`));
            }).catch(() => {
                // Do nothing
            });
    
            queueSubscriptionHelper.findInteraction(itemId).then((interactionId) => {
                if (interactionId !== undefined) {
                    return reject(new Error(`Found the interaction ${interactionId} with item Id ${itemId}. Already interaction exist`));
                }
            });
    
            function sendHandleNotificationCallback(status) {
                logger.info('Hanlde Notification responseCode = ' + status);
                resolve(true);
            }
            var payload = {
                'objectId': 'SFOR_CreateInteraction',
                'eventId': 'SFOR_CreateInteraction',
                'data': [xmlString]
            };
            queueSubscriptionHelper.setObjectInProcessing(itemId);
            httpRequestHelper.sendRequest('POST', '/system/handler-notification', payload, sendHandleNotificationCallback);
        });
    };

    function canWorkgroupQueueEmails(workgroupName) {
        return new Promise((resolve, reject) => {
            let payload = '',
                requestUri = '/configuration/workgroups/' + workgroupName + '?select=isAcdEmailRoutingActive,queueType';
    
            function workgroupConfigurationResponseCallback(status, jsonResponse) {
                if (!httpRequestHelper.isSuccessStatus(status)) {
                    reject(new Error(`Failed workgroup queue emails config, Response Status ${status}`));
                }

                if (!jsonResponse.isAcdEmailRoutingActive || !allowedQueueTypes.has(jsonResponse.queueType)) {
                    reject(new Error(`Wrong workgroup queue emails config, ACD Email Routing is ${jsonResponse.isAcdEmailRoutingActive}`));
                }

                resolve(true);
            }
    
            httpRequestHelper.sendRequest('GET', requestUri, payload, workgroupConfigurationResponseCallback);
        });
    }

    exports.icwsGetConnection = function() {
        var payload = '';
        logger.info('inside icwsGetConnection');
        function sendGetConnectionCallback(status, jsonResponse) {
            logger.info('status = ' + status);
            logger.info('response = ' + JSON.stringify(jsonResponse));
        }
        if (httpRequestHelper.isConnected()) {
            httpRequestHelper.sendRequest('GET', '/connection', payload, sendGetConnectionCallback);
        }
    };

    /**
     * Disconnects the current ICWS session, if any.
     */
    exports.icwsDisconnect = function() {
        var payload = '';

        // If there is currently an ICWS connection, then disconnect it.
        if (exports.isConnected()) {
            logger.info('Disconnect from ICWS, Close the connection to the ICWS server.');
            queueSubscriptionHelper.unsubscribeAllWorkgroupQueues();
            httpRequestHelper.sendRequest('DELETE', '/connection', payload, function(status) {
                // Ignore log out failures.
                logger.info('Disconnect status = ' + status);
                // Clear the cached ICWS credentials for the current ICWS session.
                httpRequestHelper.clearCredentials();
                if (eventSource) {
                    eventSource.close();
                    logger.info('closing event source. Ready state = ' + eventSource.readyState);
                    eventSource = null;
                }
            });
        }
    };

    exports.findAndDisconnectInteraction = function(itemId) {
        queueSubscriptionHelper.findInteraction(itemId).then((interactionId) => {
            if (interactionId !== undefined) {
                logger.info(`Found the interaction ${interactionId} with item Id ${itemId}. Interaction Disconnect`);
                queueSubscriptionHelper.icwsInteractionDisconnect(interactionId).then((status) => {
                    logger.info(`Successfully disconnected the interaction ${interactionId}, status code ${status}`);
                }).catch((err) => {
                    logger.error(err.message);
                });
            } else {
                logger.info('Interaction ID is undefined');
            }
        });
    };

    // Initialize an internal message callback only once.
    var messageCallbacksInitialized = false;

    /**
    * Initialize monitoring of connection state messages.
    */
    function initializeMessageCallbacks() {
        if (!messageCallbacksInitialized) {
            // Subscribe to the session model's callback mechanism for receiving ICWS messages.
            // The session module itself handles connectionStateChanged messages, invoking the disconnectCallback passed in to icwsConnect.
            exports.registerMessageCallback('urn:inin.com:connection:connectionStateChangeMessage', connectionStateChanged);
            exports.registerMessageCallback('urn:inin.com:queues:queueContentsMessage', queueSubscriptionHelper.processQueueContentMessage);
            messageCallbacksInitialized = true;
        }
    }

    /**
     * Connection state changed message processing callback.
     * @param {Object} jsonMessage The JSON message payload.
     * @param {String} jsonMessage.newConnectionState The new ICWS connection state.
     * @param {String} jsonMessage.reason The reason for the change.
     */
    function connectionStateChanged(jsonMessage) {
        var newConnectionState = jsonMessage.newConnectionState;
        logger.info('Inside Connection State Changed, New connection state is ' + newConnectionState);
        // If the connection changes to down, and the application state shows that it is currently connection.
        if (newConnectionState === 2) {
            if (exports.isConnected()) {
                // Stop message processing for the current ICWS session.
                stopMessageProcessing();
                // Clear the cached ICWS credentials for the current ICWS session.
                httpRequestHelper.clearCredentials();

                // If there is a cached disconnect callback, invoke it.
                if (sessionDisconnectCallback) {
                    sessionDisconnectCallback(jsonMessage.reason, jsonMessage.shouldReconnect);
                }
            }
        }
    }

    //
    // ICWS messaging support
    //

    // Dictionary of ICWS message __type ID to the callback (type: icwsMessageCallback) to invoke when that message is received.
    var icwsMessageCallbacks = {};
    // Optional callback for processing unhandled ICWS messages.
    // Type: icwsMessageCallback
    var icwsUnhandledMessageCallback = null;
    // Timer for when short-polling is used.
    var messageProcessingTimerId;

    // Polling interval for retrieving ICWS message queue.
    var ICWS_MESSAGE_RETRIEVAL_INTERVAL_MS = 1000;

    /**
     * The callback for receiving messages due to using {@link registerMessageCallback}.
     * @callback icwsMessageCallback
     * @param {Object} jsonMessage The JSON message payload.
     * @see connection.registerMessageCallback
     */

    /**
     * Sets the callback for a particular type of ICWS message.
     * @param {String} messageType The ICWS message type. (ex: urn:inin.com:status:userStatusMessage)
     * @param {icwsMessageCallback} messageCallback The callback to invoke with the message details.
     * @throws {Error} The messageCallback was undefined.
     * @throws {Error} A callback is already registered for the specified messageType.
     */
    exports.registerMessageCallback = function(messageType, messageCallback) {
        if (messageCallback === undefined) {
            throw new Error('Invalid argument "messageCallback".');
        }

        if (!icwsMessageCallbacks[messageType]) {
            icwsMessageCallbacks[messageType] = messageCallback;
        } else {
            throw new Error('Message callback already registered for message type: ' + messageType);
        }
    };

    /**
     * Starts the message processing mechanism, if not already running.
     * @see stopMessageProcessing
     */
    function startMessageProcessing() {
        // Check to see if the browser being used supports EventSource, and check
        // if the connected ICWS session supports server sent events.  If they are
        // both supported, then we will elect to use the message processing for
        // server sent events instead of short-polling.
        if (typeof EventSource !== 'undefined'
            && icwsCurrentMessagingVersion >= icwsMessagingVersionForServerSentEvents) {
            startServerSentEventsMessageProcessing();
        } else {
            startShortPollingMessageProcessing();
        }
    }

    /**
     * Starts the message processing mechanism for server sent events, if not already running.
     * @see startMessageProcessing
     */
    function startServerSentEventsMessageProcessing() {
        if (!eventSource) {
            var messagesUrl = httpRequestHelper.icwsGetRootRequestUri(httpRequestHelper.getSessionServer()) + '/' + httpRequestHelper.getSessionId() + '/messaging/messages';

            var headers = {
                'Cookie': httpRequestHelper.getSessionCookies()
            };
            eventSource = new EventSource(messagesUrl, {
                headers: headers
            });

            // Add in some event handlers to display the status of the EventSource socket.
            eventSource.onopen = function() {
                logger.info('EventSource socket was opened.');
            };
            eventSource.onerror = function(e) {
                var status;

                switch (eventSource.readyState) {
                    case EventSource.CONNECTING:
                        status = 'EventSource socket is reconnecting.';
                        break;
                    case EventSource.CLOSED:
                        status = 'EventSource socket was closed.';
                        break;
                    default:
                        throw e;
                }

                logger.error('Event Source Status = ' + status);
            };

            eventSource.addEventListener('message', function(e) {
                logger.info('Received Message = ' + e.data);
                var message = JSON.parse(e.data);
                processMessage(message);
            });
        }
    }

    /**
     * Starts the message processing mechanism for short-polling, if not already running.
     * @see startMessageProcessing
     */
    function startShortPollingMessageProcessing() {
        // Only send the next request once the previous result has been received.
        function runTimerInstance() {
            messageProcessingTimerCallback();

            messageProcessingTimerId = setTimeout(runTimerInstance, ICWS_MESSAGE_RETRIEVAL_INTERVAL_MS);
        }

        if (!messageProcessingTimerId) {
            runTimerInstance();
        }
    }

    /**
     * Stops the message processing mechanism, if running.
     * @see startMessageProcessing
     */
    function stopMessageProcessing() {
        // Call the appropriate stop based on if we used server sent events or short-polling.
        if (eventSource) {
            stopServerSentEventsMessageProcessing();
        } else {
            stopShortPollingMessageProcessing();
        }
    }

    /**
     * Stops the message processing mechanism for server sent events, if running.
     * @see startMessageProcessing
     * @see stopMessageProcessing
     */
    function stopServerSentEventsMessageProcessing() {
        if (eventSource) {
            eventSource.close();
            logger.info('closing event source. Ready state = ' + eventSource.readyState);
            eventSource = null;
        }
    }

    /**
     * Stops the message processing mechanism for short-polling, if running.
     * @see startMessageProcessing
     * @see stopMessageProcessing
     */
    function stopShortPollingMessageProcessing() {
        if (messageProcessingTimerId) {
            clearTimeout(messageProcessingTimerId);
            messageProcessingTimerId = null;
        }
    }

    /**
     * Implements the message processing mechanism timer callback.
     * @see startMessageProcessing
     * @see stopMessageProcessing
     */
    function messageProcessingTimerCallback() {
        var currentSessionId, payload, messageIndex, messageCount;

        // Provide contextual information for the request.
        logger.info('Retrieve messages from ICWS', 'Retrieve any pending event messages from the ICWS server.');

        currentSessionId = httpRequestHelper.getSessionId();

        payload = {};
        httpRequestHelper.sendRequest('GET', '/messaging/messages', payload, function(status, jsonResponse, sessionId) {
            // Ignore results for an older session.
            if (currentSessionId === sessionId) {
                if (httpRequestHelper.isSuccessStatus(status)) {
                    // Process retrieved messages.
                    for (messageIndex = 0, messageCount = jsonResponse.length; messageIndex < messageCount; messageIndex++) {
                        processMessage(jsonResponse[messageIndex]);
                    }
                }
            }
        });
    }

    /**
     * Calls the registered callback for a message received from the server.
     * @see startMessageProcessing
     * @see stopMessageProcessing
     */
    function processMessage(jsonMessage) {
        var messageType, messageCallback;
        messageType = jsonMessage.__type;

        // For each message, invoke a registered message callback if there is one;
        // otherwise, invoke the unhandled message callback.
        messageCallback = icwsMessageCallbacks[messageType];
        if (messageCallback) {
            messageCallback(jsonMessage);
        } else if (icwsUnhandledMessageCallback !== null) {
            icwsUnhandledMessageCallback(jsonMessage);
        }
    }

    exports.testGetMessageCallbacksMethod = function() {
        return icwsMessageCallbacks['urn:inin.com:connection:connectionStateChangeMessage'];
    };

    exports.testGetEventSourceStatus = function() {
        return eventSource.readyState;
    };

    return exports;
// eslint-disable-next-line no-use-before-define
}(icwsApiMethod || {}));

module.exports = icwsApiMethod;
