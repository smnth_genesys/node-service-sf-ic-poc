const uuid = require('uuid/v1');
const httpRequestHelper = require('./httpRequestHelper');
const logger = require('../UTILS/trace')('IC-Common.queueSubscriptionHelper');

var queueSubscriptionHelper = (function(exports) {
    // Subscription IDs list against the workgroup
    var workgroupSubscriptionIdMap = {};
    // Mapping of each salesforce item ID to interactionId
    var itemIdInteractionMap = {};
    // Object processing map
    var objectProcessingMap = {};
    // DisconnectOptions Meaning, I = Internal Disconnect, E = External Disconnect, X = Interaction Suspended
    let disconnectOptionsSet = new Set(['I', 'E', 'X']);

    exports.getSubscribedWorkgroups = function() {
        return Object.keys(workgroupSubscriptionIdMap);
    };

    exports.isObjectInProcessing = function(itemId) {
        return new Promise((resolve, reject) => {
            logger.info('Inside isObjectInProcessing');

            const val = objectProcessingMap[itemId];
            logger.info(`Value of object processing queue ${val}`);
            if (val !== undefined && val === 1) {
                return resolve();
            }
            // eslint-disable-next-line prefer-promise-reject-errors
            return reject();
        });
    };

    exports.setObjectInProcessing = function(itemId) {
        objectProcessingMap[itemId] = 1;
        logger.info(`Set Object in process = ${Object.keys(objectProcessingMap)} and ${Object.values(objectProcessingMap)}`);
    };
    
    exports.icwsQueueSubscription = function(workgroupName) {
        return new Promise((resolve, reject) => {
            // QueueType is 2 for workgroup queues
            const queueType = 2;
        
            let existingSubscriptionId = workgroupSubscriptionIdMap[workgroupName];
            if (existingSubscriptionId !== undefined) {
                logger.info(`Workgroup ${workgroupName} already susbcribed`);
                return resolve(true);
            }
            var subscriptionId = uuid();
            var payload = {
                queueIds: [
                    {
                        queueType: queueType,
                        queueName: workgroupName
                    }
                ],
                attributeNames: ['SF_URLPop', 'Eic_State']
            };

            function queueSubscriptionSuccessCallback(status) {
                if (httpRequestHelper.isSuccessStatus(status)) {
                    workgroupSubscriptionIdMap[workgroupName] = subscriptionId;
                    logger.info(`Subscribed to Queue ${workgroupName} with subscription ID ${subscriptionId}`);
                    resolve(true);
                } else {
                    reject(new Error(`Failed to subscribe to the workgroup ${workgroupName} with subscription ID ${subscriptionId}`));
                }
            }

            httpRequestHelper.sendRequest('PUT', '/messaging/subscriptions/queues/' + subscriptionId, payload, queueSubscriptionSuccessCallback);
        });
    };
    
    function getItemIdFromInteractionId(value) {
        return Object.keys(itemIdInteractionMap).find(key => itemIdInteractionMap[key] === value);
    }
    
    exports.processQueueContentMessage = function(jsonMessage) {
        logger.info('Processing QueueContentMessage....');
        let createdInteractions = jsonMessage.interactionsAdded;
        let changedInteractions = jsonMessage.interactionsChanged;
        let deletedInteractions = jsonMessage.interactionsRemoved;
        
        // Verify the subscriptionId with subscribed queue
        var subscriptionIds = Object.values(workgroupSubscriptionIdMap);
        if (!subscriptionIds.includes(jsonMessage.subscriptionId)) {
            logger.warn(`queue content message is not for salesforce integration susbcription. subscription ID is ${jsonMessage.subscriptionId}`);
            return;
        }

        if (createdInteractions !== undefined && createdInteractions.length > 0) {
            for (let index in createdInteractions) {
                if (createdInteractions[index]) {
                    let interaction = createdInteractions[index];
                    let itemId = interaction.attributes.SF_URLPop;
                    if (itemId && !disconnectOptionsSet.has(interaction.attributes.Eic_State)) {
                        // Disconnect already existing interaction for the itemId
                        let existingInteractionId = itemIdInteractionMap[itemId];
                        if (existingInteractionId !== undefined && existingInteractionId !== interaction.interactionId) {
                            exports.icwsInteractionDisconnect(existingInteractionId).then((status) => {
                                logger.info(`Successfully disconnected the interaction ${existingInteractionId} with item Id ${itemId}, status code ${status}`);
                            }).catch((err) => {
                                logger.error(err.message);
                            });
                        }
                        itemIdInteractionMap[itemId] = interaction.interactionId;
                        // Remove the object from processing map, It is processed
                        delete objectProcessingMap[itemId];
                        logger.info(`Item Id of the current interaction = ${itemId}`);
                    }
                }
            }
        }

        if (changedInteractions !== undefined && changedInteractions.length > 0) {
            let interactionIds = Object.values(itemIdInteractionMap);
            for (let index in changedInteractions) {
                if (changedInteractions[index]) {
                    let interaction = changedInteractions[index];
                    if (interactionIds.includes(interaction.interactionId) && disconnectOptionsSet.has(interaction.attributes.Eic_State)) {
                        let itemId = getItemIdFromInteractionId(interaction.interactionId);
                        delete itemIdInteractionMap[itemId];
                    }
                }
            }
        }

        if (deletedInteractions !== undefined && deletedInteractions.length > 0) {
            for (let index in deletedInteractions) {
                if (deletedInteractions[index]) {
                    let itemId = getItemIdFromInteractionId(deletedInteractions[index]);
                    if (typeof itemId !== 'undefined') {
                        delete itemIdInteractionMap[itemId];
                    }
                }
            }
        }
    };

    exports.findInteraction = function(itemId) {
        return new Promise((resolve) => {
            let interactionId = itemIdInteractionMap[itemId];
            return resolve(interactionId);
        });
    };

    exports.subscribeWorkgroupQueues = function(workgroups) {
        // Re initialize the data maps
        workgroupSubscriptionIdMap = {};
        objectProcessingMap = {};

        if (workgroups.length > 0) {
            for (let index in workgroups) {
                if (workgroups[index]) {
                    let workgroup = workgroups[index];
                    if (workgroup) {
                        workgroup = workgroup.trim();
                        exports.icwsQueueSubscription(workgroup).then((status) => {
                            logger.info(`Workgourp ${workgroup} queue subscription status is ${status}`);
                        }).catch((err) => {
                            logger.error(err.message);
                        });
                    }
                }
            }
        }
    };

    function icwsQueueUnsubscription(subscriptionId) {
        return new Promise((resolve, reject) => {
            var payload = '';
        
            function queueUnsubscriptionSuccessCallback(status) {
                if (httpRequestHelper.isSuccessStatus(status)) {
                    resolve(true);
                } else {
                    // eslint-disable-next-line prefer-promise-reject-errors
                    reject(false);
                }
            }
            httpRequestHelper.sendRequest('DELETE', '/messaging/subscriptions/queues/' + subscriptionId, payload, queueUnsubscriptionSuccessCallback);
        });
    }

    exports.unsubscribeAllWorkgroupQueues = function() {
        var subscriptionIds = Object.values(workgroupSubscriptionIdMap);
        if (subscriptionIds.length === 0) {
            logger.info('Current Subscribed Queue list is empty. Nothing to unsubscribe');
            return;
        }
        for (var index in subscriptionIds) {
            if (subscriptionIds[index]) {
                // eslint-disable-next-line no-loop-func
                icwsQueueUnsubscription(subscriptionIds[index]).then(() => {
                    logger.info(`Unsubscribe the subscription ID ${subscriptionIds[index]}`);
                // eslint-disable-next-line no-loop-func
                }).catch((err) => {
                    logger.error(`Failed to unsubscribe the subscription ID ${subscriptionIds[index]}, error: ${err}`);
                });
            }
        }
        workgroupSubscriptionIdMap = {};
        itemIdInteractionMap = {};
    };

    exports.icwsInteractionDisconnect = function(interactionId) {
        return new Promise((resolve, reject) => {
            var payload = '';

            httpRequestHelper.sendRequest('POST', '/interactions/' + interactionId + '/disconnect', payload, function(status) {
                if (httpRequestHelper.isSuccessStatus(status)) {
                    resolve(true);
                } else {
                    reject(new Error(`Failed to disconnect the interaction ${interactionId}, status code ${status}`));
                }
            });
        });
    };
    return exports;
// eslint-disable-next-line no-use-before-define
}(queueSubscriptionHelper || {}));

module.exports = queueSubscriptionHelper;
