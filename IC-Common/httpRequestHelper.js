const { XMLHttpRequest } = require('xmlhttprequest-cookie');
const logger = require('../UTILS/trace')('IC-Common.httpRequestHelper');

var httpRequestHelper = (function(exports) {
    //
    // ICWS request support
    //

    // Constants for ICWS server access.
    const ICWS_URI_SCHEME = 'http://';
    const ICWS_URI_PORT = '8018';
    const ICWS_URI_PATH = '/icws';
    const ICWS_MEDIA_TYPE = 'application/vnd.inin.icws+JSON';
    const ICWS_MEDIA_CHARSET = 'charset=utf-8';

    // Timeout constant for web service requests.
    const ICWS_REQUEST_TIMEOUT_MS = 60000;

    //
    // Credential storage
    //

    // Stores values for the currently connected ICWS session.
    //   server The ICWS server with which the ICWS session was established.
    //   userId The IC user ID that was used to login.
    //   csrfToken The ICWS session's CSRF token.
    //   sessionId The ICWS session's session ID.  This value must be passed in with every request.
    let icwsCurrentSession = null;

    // Stores the value of current ICWS session cookie for event source
    let icwsCurrentSessionCookies = null;

    /**
   * Gets the current ICWS session Cookies, if any.
   * @returns {String} The ICWS connect session cookie, or null if not connected.
   */
    exports.getSessionCookies = function() {
        return icwsCurrentSessionCookies;
    };

    /**
   * Determines whether an ICWS session is connected.
   * @returns {Boolean} true if an ICWS session is connected; otherwise, false.
   */
    exports.isConnected = function() {
    // Convert the session ID to a boolean to determine if we're connected.
        return !!icwsCurrentSession;
    };

    /**
     * Determines whether an http status code is in the successful range (200-299).
     * @param {Number} statusCode The status code to check.
     * @returns {Boolean} true if the statusCode represents a success.
     */
    exports.isSuccessStatus = function(statusCode) {
        return ((statusCode >= 200) && (statusCode <= 299));
    };

    /**
   * Gets the current ICWS session server, if any.
   * @returns {String} The IC server for the session, or null if not connected.
   */
    exports.getSessionServer = function() {
        return icwsCurrentSession && icwsCurrentSession.server;
    };

    /**
   * Gets the current ICWS session ID, if any.
   * @returns {String} The ICWS session ID, or null if not connected.
   */
    exports.getSessionId = function() {
        return icwsCurrentSession && icwsCurrentSession.sessionId;
    };

    /**
   * Stores a set of ICWS session credentials.
   * @param {String} icwsServer The server name where ICWS is available.
   * @param {String} icwsUserId The IC user ID for the session.
   * @param {String} icwsCsrfToken The ICWS CSRF token.
   * @param {String} icwsSessionId The ICWS session ID.
   */
    exports.setCredentials = function(icwsServer, icwsUserId, icwsCsrfToken, icwsSessionId) {
        icwsCurrentSession = {
            server: icwsServer,
            userId: icwsUserId,
            csrfToken: icwsCsrfToken,
            sessionId: icwsSessionId
        };
    };

    /**
   * Clears the current ICWS session credentials, abandoning the session, if any.
   */
    exports.clearCredentials = function() {
        icwsCurrentSession = null;
        // eslint-disable-next-line no-undef
        icwsCurrentMessagingVersion = null;
        // eslint-disable-next-line no-undef
        icwsEffectiveStationId = null;
        icwsCurrentSessionCookies = null;
    };

    /**
   * Retrieves the base URI to an ICWS server.
   * @param {String} [optServer] The server name where ICWS is available.  Defaults to the active connection's server.
   * @returns {String} The base URI, or empty string if there is no active connection.
   */
    function icwsGetRootUri(optServer) {
    // If a server parameter was not provided, and there is an active ICWS session, use that session's server.
        if (!optServer && exports.isConnected()) {
            optServer = icwsCurrentSession.server;
        }

        // If an ICWS server was found, compose the URI root for that server.
        if (optServer) {
            return ICWS_URI_SCHEME + optServer + ':' + ICWS_URI_PORT;
        }

        return '';
    }

    /**
   * Retrieves the base URI for an ICWS request.
   * @param {String} [optServer] The server name where ICWS is available.
   * @returns {String} The base URI, or empty string if there is no active connection.
   */
    exports.icwsGetRootRequestUri = function(optServer) {
    // Get the base URI for the specified ICWS server.
        var uri = icwsGetRootUri(optServer);

        // If there is a base URI, compose it with the ICWS web service path.
        if (uri) {
            return uri + ICWS_URI_PATH;
        }

        return '';
    };

    /**
   * Retrieves the base URI for a request for a non-ICWS resource.
   * @returns {String} The base URI, or empty string if there is no active connection.
   */
    exports.icwsGetRootResourceUri = function() {
        return icwsGetRootUri();
    };

    /**
   * The callback for receiving the result from {@link sendRequest} or {@link sendSessionlessRequest}.
   * @callback resultCallback
   * @param {Number} status The HTTP status code of the response.
   * @param {Object} jsonResponse The JSON response payload.
   * @param {String} correlationId A correlation ID to associate diagnostic messages.
   * @param {String} sessionId The ICWS session ID.
   * @see connection.sendRequest
   * @see connection.sendSessionlessRequest
   */

    /**
   * Sends a request that doesn't require an active ICWS session.
   * @param {String} server The server name where ICWS is available.
   * @param {String} method The HTTP method of the request. (ex: GET, POST, PUT, DELETE)
   * @param {String} requestPath The uri fragment for the request.  The part after the sessionId template parameter.  (ex: /messaging/messages)
   * @param {Object|String} payload The payload to send with the request, as a string or JSON.
   * @param {resultCallback} resultCallback The callback to invoke with the response details.
   * @returns {Number} The correlation ID of the request, to support synchronization with the value passed to resultCallback.
   * @see connection.sendRequest
   * @throws {Error} The resultCallback was undefined.
   */
    exports.sendSessionlessRequest = function(server, method, requestPath, payload, resultCallback) {
        return sendRequestImpl(server, method, requestPath, payload, resultCallback, true);
    };

    /**
   * Sends a request to an existing ICWS session.
   * @param {String} method The HTTP method of the request. (ex: GET, POST, PUT, DELETE)
   * @param {String} requestPath The uri fragment for the request.  The part after the sessionId template parameter.  (ex: /messaging/messages)
   * @param {Object|String} payload The payload to send with the request, as a string or JSON.
   * @param {resultCallback} resultCallback The callback to invoke with the response details.  If there is no existing session, the callback will be invoked with a 0 status and empty object.
   * @returns {Number} The correlation ID of the request, to support synchronization with the value passed to resultCallback, or null if there is no connected session.
   * @see connection.sendSessionlessRequest
   * @throws {Error} The resultCallback was undefined.
   */
    exports.sendRequest = function(method, requestPath, payload, resultCallback) {
        if (exports.isConnected()) {
            return sendRequestImpl(icwsCurrentSession.server, method, requestPath, payload, resultCallback, false);
        }
        return null;
    };

    // For application diagnostic purposes, track a numeric correlation ID to associate requests with responses.
    var nextCorrelationId = 1;

    /**
   * Implementation for sending a request to ICWS.
   * @param {String} server The server name where ICWS is available.
   * @param {String} method The HTTP method of the request. (ex: GET, POST, PUT, DELETE)
   * @param {String} requestPath The uri fragment for the request.  The part after the sessionId template parameter.  (ex: /messaging/messages)
   * @param {Object|String} payload The payload to send with the request, as a string or JSON.
   * @param {resultCallback} resultCallback The callback to invoke with the response details.
   * @param {Boolean} [optExcludeSessionId=false] Specifies whether to exclude the existing session ID from the request (i.e. whether to send a sessionless request).
   * @returns {Number} The correlation ID of the request, to support synchronization with the value passed to resultCallback.
   * @throws {Error} The resultCallback was undefined.
   */
    function sendRequestImpl(server, method, requestPath, payload, resultCallback, optExcludeSessionId) {
        var correlationId, sessionId, xmlHttp, uri;

        if (resultCallback === undefined) {
            throw new Error('Invalid argument "resultCallback".');
        }

        // Allow JSON to be provided as an option, then convert it to a string.
        if (typeof payload !== 'string' && !(payload instanceof String)) {
            payload = JSON.stringify(payload);
        }

        // For application diagnostic purposes, track a numeric correlation ID to associate requests with responses.
        correlationId = nextCorrelationId++;
        sessionId = '0';
        // Once a session has been established, subsequent requests for that session require its session ID.
        if (!optExcludeSessionId) {
            sessionId = icwsCurrentSession.sessionId;
        }

        // Use an XHR to make the web service request.
        xmlHttp = new XMLHttpRequest();

        // Once it's available, process the request response.
        xmlHttp.onreadystatechange = function() {
            if (xmlHttp.readyState === 4) {
                if (optExcludeSessionId && exports.isSuccessStatus(xmlHttp.status)) {
                    icwsCurrentSessionCookies = xmlHttp.getResponseHeader('Set-Cookie');
                    logger.info('session cookie = ' + icwsCurrentSessionCookies);
                }
                sendRequestCompleted(xmlHttp, server, resultCallback);
            }
        };

        // Create the base URI, using the ICWS port, with the specified server and session ID.
        // This helper function abstracts away the details.
        uri = exports.icwsGetRootRequestUri(server);
        // Once a session has been established, subsequent requests for that session require its session ID.
        if (!optExcludeSessionId) {
            uri += '/' + sessionId;
        }
        // Add the specific ICWS request to the URI being built.
        if (requestPath.substring(0, 1) !== '/') {
            uri += '/';
        }
        uri += requestPath;

        // Open the HTTP connection.
        xmlHttp.open(method, uri, true);

        // Specify that credentials should be used for the request, in order to work correctly with CORS.
        xmlHttp.withCredentials = true;

        xmlHttp.timeout = ICWS_REQUEST_TIMEOUT_MS;

        // If the ICWS request is a session-based request, then the session's CSRF token must be set as
        // a header parameter.
        if (!optExcludeSessionId) {
            xmlHttp.setRequestHeader('ININ-ICWS-CSRF-Token', icwsCurrentSession.csrfToken);
        }
        // The ICWS content-type must be specified.
        xmlHttp.setRequestHeader('Content-type', ICWS_MEDIA_TYPE + ';' + ICWS_MEDIA_CHARSET);
        xmlHttp.setRequestHeader('accept-language', 'en-US');

        // Send the request.
        xmlHttp.send(payload);

        logger.info('Send Request, Payload: ' + JSON.stringify(payload));

        return correlationId;
    }

    /**
   * Process the response to an ICWS request.
   * @param {Object} xmlHttp The XMLHttpRequest instance for the request.
   * @param {String} server The server name to which session is connected, if any, for diagnostic purposes.
   * @param {Number} correlationId The correlation ID of the request, for diagnostic purposes.
   * @param {resultCallback} resultCallback The callback to invoke with the result.
   */
    function sendRequestCompleted(xmlHttp, server, resultCallback) {
        var status, responseText, response;

        status = xmlHttp.status;
        logger.info('Inside sendRequestCompleted');
        // Handle 401 failures as server disconnects.
        if (status === 401) {
            logger.error('401 error response' + JSON.stringify(xmlHttp.responseText));
        }

        // Process the response body.
        responseText = xmlHttp.responseText;
        if (responseText) {
            try {
                response = JSON.parse(responseText);
            } catch (e) {
                /* If the JSON cannot be parsed, use an empty object for response. */
                logger.error(`Unable to parse response text: ${responseText}`);
                response = {};
            }
        } else {
            response = {};
        }

        // Signal the request result to the caller's callback.
        resultCallback(status, response, server);
    }
    return exports;
// eslint-disable-next-line no-use-before-define
}(httpRequestHelper || {}));
module.exports = httpRequestHelper;
