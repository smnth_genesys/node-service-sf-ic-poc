---

1. Clone the repo.
2. Run npm install from the root directory.
3. Run node app.js to make server up.


# For Developers
- Run "npm test" to run the unit test cases 
- "npm run test-report" to run the code coverage.

---