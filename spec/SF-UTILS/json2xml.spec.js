let js2xmlparser = require('js2xmlparser'),
    sinon = require('sinon');

describe('Convert JSON obj to XML String', () => {
    let j2x, j2xParserSpy;
    beforeAll(() => {
        j2xParserSpy = sinon.spy(js2xmlparser, 'parse');
        j2x = require('../../SF-UTILS/json2xml');
    });
    it('should be funtion type', () => {
        expect(typeof j2x).toEqual('function');
    });
    describe('When empty data is sent', () => {
        let xmlData;
        beforeEach(() => {
            xmlData = j2x();
        });
        it('should return undefined', () => {
            expect(xmlData).toBeUndefined();
        });
    });
    describe('When data is sent', () => {
        let xmlData, json;
        beforeEach(() => {
            json = {
                'ObjectRouting__ItemId__c': '23424'
            };
            xmlData = j2x(json);
        });
        it('should call js2xmlparser\'s parse method', () => {
            sinon.assert.calledOnce(j2xParserSpy);
        });
        it('should contain xml data', () => {
            expect(xmlData).toContain('<?xml');
        });
    });
    describe('When data has remoteName', () => {
        let xmlData, json;
        beforeEach(() => {
            sinon.resetHistory();
            json = {
                'ObjectRouting__ItemId__c': '23424',
                'ObjectRouting__SearchableID__c': '000444'
            };
            xmlData = j2x(json);
        });
        it('should call js2xmlparser\'s parse method', () => {
            sinon.assert.calledOnce(j2xParserSpy);
        });
        it('should contain xml data and Case: 000444', () => {
            expect(xmlData).toContain('<?xml');
            expect(xmlData).toContain('Case: 000444');
        });
    });
});
