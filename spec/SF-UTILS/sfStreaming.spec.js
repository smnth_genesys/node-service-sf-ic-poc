let sinon = require('sinon'),
    jsForce = require('jsforce');

describe('Salesforce Streaming api', () => {
    let sf, mockJSForceLogin;
    beforeAll(() => {
        // Jasmine.createSpy(jsForce.Connection.prototype, 'streaming');
        mockJSForceLogin = sinon.stub(jsForce.Connection.prototype, 'login').callsFake((username, password, loginCallback) => {
            if (username && password) {
                loginCallback(false, true);
            }
        });
        sf = require('../../SF-UTILS/sfStreaming');
    });
    describe('When login method is called', () => {
        beforeEach(() => {
            sf.loginSf();
        });
        it('should have called jsForce login method', () => {
            sinon.assert.calledOnce(mockJSForceLogin);
        });
    });
});
