let Request = require('request'),
    sinon = require('sinon');
const icwsConnect = require('../IC-Common/icwsApiMethod');
const appMessage = 'This is a connector/service between pureconect and salesforce. This is under development!';

describe('Server', () => {
    let server, spymethod;
    beforeAll(() => {
        spymethod = sinon.spy(icwsConnect, 'icwsConnect');
        server = require('../app');
    });
    // eslint-disable-next-line no-undef
    afterAll(() => {
        server.close();
    });

    describe('When server is listening on the port', () => {
        it('should call the method to connect to the ic server', () => {
            sinon.assert.calledOnce(spymethod);
        });
    });

    describe('When Get is called', () => {
        var data = {};
        beforeAll((done) => {
            Request.get('http://localhost:5002/', (err, res, body) => {
                data.status = res.statusCode;
                data.body = body;
                done();
            });
        });
        it('Status should be 200', () => {
            expect(data.status).toBe(200);
        });
        it('Message should be ', () => {
            expect(data.body).toEqual(appMessage);
        });
    });

    describe('When unknown get is called', () => {
        var data = {};
        beforeAll((done) => {
            Request.get('http://localhost:5002/test', (err, res) => {
                data.status = res.statusCode;
                done();
            });
        });
        it('Status should be 404', () => {
            expect(data.status).toBe(404);
        });
    });
});
