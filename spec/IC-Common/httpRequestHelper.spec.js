require('dotenv').config();
const path = require('path');
const { IC_USERNAME, IC_PWD, IC_SERVER } = require(path.join(process.cwd(), process.argv[2]));

const appName = 'Salesforce_IC_Integration';

describe('Testing the ICWS request helper method', () => {
    var requestHelper, responseStatus, responseData;
    beforeAll(() => {
        requestHelper = require('../../IC-Common/httpRequestHelper');
    });
    // eslint-disable-next-line no-undef
    afterAll((done) => {
        requestHelper.sendRequest('DELETE', '/connection', '', (status) => {
            // eslint-disable-next-line no-console
            console.log('Disconnect status = ' + status);
            requestHelper.clearCredentials();
            return done();
        });
    });

    describe('Send connection request with InCorrect user name', () => {
        beforeAll((done) => {
            var payload = {
                __type: 'urn:inin.com:connection:icAuthConnectionRequestSettings',
                applicationName: appName,
                userID: IC_USERNAME + 'undefined',
                password: IC_PWD
            };
            requestHelper.sendSessionlessRequest(IC_SERVER, 'POST', '/connection', payload, (status, response) => {
                responseStatus = status;
                responseData = response;
                return done();
            });
        });
        it('Status should be 400', () => {
            expect(responseStatus).toBe(400);
        });
        it('Error response message should be authentication failure', () => {
            expect(responseData.message).toBe('The authentication process failed.');
        });
    });

    describe('Send connection request with correct login details', () => {
        beforeAll((done) => {
            var payload = {
                __type: 'urn:inin.com:connection:icAuthConnectionRequestSettings',
                applicationName: appName,
                userID: IC_USERNAME,
                password: IC_PWD
            };
            requestHelper.sendSessionlessRequest(IC_SERVER, 'POST', '/connection', payload, (status, response) => {
                responseStatus = status;
                responseData = response;
                requestHelper.setCredentials(IC_SERVER, IC_USERNAME, response.csrfToken, response.sessionId);
                return done();
            });
        });

        it('Status should be 201', () => {
            expect(responseStatus).toBe(201);
        });
        it('Session ID should be part of response data', () => {
            expect(responseData.sessionId).toBeDefined();
        });
    });
});
