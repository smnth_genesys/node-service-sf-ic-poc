const path = require('path');
require('dotenv').config();
let credentials = require('./UTILS/credentials');
const { IC_APP, IC_PWD, IC_SERVER, IC_USERNAME } = require(path.join(process.cwd(), process.argv[2]));
const appName = 'SF_IC_Integration_UnitTest';
describe('Testing for ICWS API Methods logic', () => {
    var icwsApiMethod, success, result;
    beforeAll((done) => {
        icwsApiMethod = require('../../IC-Common/icwsApiMethod');
        return done();
    });
    // eslint-disable-next-line no-undef
    afterAll((done) => {
        icwsApiMethod.icwsDisconnect();
        return done();
    });

    describe('ICWS connection API test', () => {
        beforeAll((done) => {
            function connectCallback(successResponse, resultResponse) {
                success = successResponse;
                result = resultResponse;
                return done();
            }
            icwsApiMethod.icwsConnect(appName, credentials.icServerName, credentials.icUserName,
                                      credentials.icPwd, connectCallback, () => {});
        });
        it('connect callback response should be success', () => {
            expect(success).toBeTruthy();
        });
        it('Response should have a valid session ID', () => {
            expect(result.icwsSessionId).toBeDefined();
        });
    });

    describe('ICWS handle notification API test', () => {
        var status;
        var workgroupName = 'workgroup1';
        var itemId = 'abcd';
        beforeAll((done) => {
            const xmlString = '<DataItem>\
                                    <InteractionType>Email</InteractionType>\
                                    <Eic_RemoteAddress>0000000</Eic_RemoteAddress>\
                                    <QueueName>DummyQueue</QueueName>\
                                    <RemoteName></RemoteName>\
                                    <SenderAddress>abc@genesys.com</SenderAddress>\
                                    <RequestId></RequestId>\
                                    <Skills></Skills>\
                                </DataItem>';
            icwsApiMethod.icwsHandleNotification(xmlString, workgroupName, itemId).then((resultStatus) => {
                status = resultStatus;
                done();
            }).catch((err) => {
                status = 0;
                done();
            });
        });
        it('Handle Notification response status should be 202', () => {
            expect(status).toBe(202);
        });
    });

    describe('ICWS messaging support for connection state changed test', () => {
        it('ICWS message callbacks method for connection state change', () => {
            var messageCallback = icwsApiMethod.testGetMessageCallbacksMethod();
            expect(messageCallback).toBeDefined();
        });
        it('Event source status should be OPENED(1)', () => {
            var eventSourceStatus = icwsApiMethod.testGetEventSourceStatus();
            expect(eventSourceStatus).toBe(1);
        });
    });
});
