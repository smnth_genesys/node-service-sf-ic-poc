module.exports = {
    'env': {
        'browser': true,
        'es6': true
    },
    'extends': 'eslint:recommended',
    'parserOptions': {
        'ecmaVersion': 2018,
        'sourceType': 'module'
    },
    plugins: [
        'connect-internal'
    ],
    'rules': {
        // Possible Errors
        // https://eslint.org/docs/rules/#possible-errors
        // --------------------------------------------
        'no-template-curly-in-string': 'error',
        // Best Practices
        // http://eslint.org/docs/rules/#best-practices
        // --------------------------------------------
        'accessor-pairs': 'error',
        'array-callback-return': 'error',
        'block-scoped-var': 'error',
        'curly': 'error',
        'default-case': 'error',
        'dot-notation': 'error',
        'eqeqeq': 'error',
        'guard-for-in': 'error',
        'no-alert': 'error',
        'no-caller': 'error',
        'no-else-return': 'error',
        // 'no-empty-function': 'warn',
        'no-eq-null': 'error',
        'no-eval': 'error',
        'no-extend-native': 'error',
        'no-extra-bind': 'error',
        'no-extra-label': 'error',
        'no-floating-decimal': 'error',
        'no-implicit-coercion': ['error', { 'allow': ['!!'] }],
        'no-implied-eval': 'error',
        'no-iterator': 'error',
        'no-labels': 'error',
        'no-lone-blocks': 'error',
        'no-loop-func': 'error',
        // 'no-magic-numbers': ['warn', { 'ignore': [-1, 1, 0] }],
        'no-multi-spaces': 'error',
        'no-new': 'error',
        'no-new-func': 'error',
        'no-new-wrappers': 'error',
        'no-octal-escape': 'error',
        // 'no-param-reassign': 'warn',
        'no-proto': 'error',
        // 'no-return-assign': 'error',
        'no-return-await': 'error', // Not in tslint.json
        'no-script-url': 'error',
        'no-self-compare': 'error',
        'no-sequences': 'error',
        'no-throw-literal': 'error',
        'no-unmodified-loop-condition': 'error',
        'no-unused-expressions': 'error',
        'no-useless-call': 'error',
        'no-useless-concat': 'error',
        'no-useless-return': 'error',
        'no-void': 'error',
        'no-with': 'error',
        'prefer-promise-reject-errors': 'error',
        'require-await': 'error',

        // Stict Mode
        // http://eslint.org/docs/rules/#strict-mode
        // ---------------------------------------
        // 'strict': 'error',

        // Variables
        // http://eslint.org/docs/rules/#variables
        // ---------------------------------------
        'no-label-var': 'error',
        // 'no-shadow': 'error',
        'no-shadow-restricted-names': 'error',
        'no-undef-init': 'error',
        //'no-undefined': 'error',
        'no-use-before-define': ['error', { 'functions': false }],

        // Node.js and CommonJS
        // http://eslint.org/docs/rules/#nodejs-and-commonjs
        // ---------------------------------------
        'no-new-require': 'error',
        'no-path-concat': 'error',

        // Stylistic Issues
        // http://eslint.org/docs/rules/#stylistic-issues
        // ----------------------------------------------
        'array-bracket-newline': ['error', 'consistent'],
        'array-bracket-spacing': ['error', 'never'],
        'array-element-newline': ['error', 'consistent'],
        'block-spacing': 'error',
        'brace-style': ['error', '1tbs', { 'allowSingleLine': true }],
        'camelcase': 'error',
        'capitalized-comments': ['error', 'always', { 'ignoreConsecutiveComments': true }],
        'comma-dangle': 'error',
        'comma-spacing': 'error',
        'comma-style': 'error',
        'computed-property-spacing': 'error',
        'eol-last': 'error',
        'func-call-spacing': 'error',
        'func-name-matching': ['error', { "considerPropertyDescriptor": true }],
        // 'function-paren-newline': ['error', 'consistent'],
        'implicit-arrow-linebreak': ['error', 'beside'],
        'indent': ['error', 4, {
            'SwitchCase': 1,
            'VariableDeclarator': 1,
            'outerIIFEBody': 1,
            'FunctionDeclaration': { 'body': 1, 'parameters': 1 },
            'FunctionExpression': { 'body': 1, 'parameters': 1 },
            'CallExpression': { 'arguments': 'first' },
            'ArrayExpression': 1,
            'ObjectExpression': 1,
            'ImportDeclaration': 1,
            'flatTernaryExpressions': false,
            'ignoreComments': false
        }],
        'key-spacing': ['error', { 'beforeColon': false, 'afterColon': true }],
        'keyword-spacing': ['error', { 'before': true, 'after': true }],
        // 'linebreak-style': ['error', 'windows'],
        // 'lines-around-comment': 'error'
        'lines-between-class-members': ['error', 'always'],
        'multiline-ternary': ['error', 'always-multiline'],
        'new-cap': ['error', {
            'newIsCap': true,
            'capIsNew': true,
            'properties': true,
            'capIsNewExceptions': [
                '$.Deferred',
                '$.Event',
                'simRunnerReporter.FireTrigger',
                'simRunnerReporter.ReportCommandError',
                'simRunnerReporter.ReportCommandComplete',
                'simRunnerReporter.ReportSessionRestartRequest',
                'simRunnerReporter.ReportPluginShutdownException'
            ],
            'newIsCapExceptions': ['Guid.createPseudoGuid'],
            'newIsCapExceptionPattern': ".*\.params"
        }],
        'new-parens': 'error',
        'no-array-constructor': 'error',
        'no-bitwise': 'error',
        'no-lonely-if': 'error',
        'no-multi-assign': 'error',
        'no-multiple-empty-lines': ['error', { 'max': 1, 'maxBOF': 0 }],
        'no-nested-ternary': 'error',
        'no-new-object': 'error',
        'no-tabs': 'error',
        'no-trailing-spaces': ['error', { 'skipBlankLines': true }],
        'no-unneeded-ternary': 'error',
        'no-whitespace-before-property': 'error',
        'object-curly-newline': ['error', { 'consistent': true }],
        // 'object-curly-spacing': ['error', 'always'],
        'object-property-newline': ['error', { 'allowAllPropertiesOnSameLine': true }],
        'operator-assignment': ['error', 'always'],
        'operator-linebreak': ['error', 'before', { overrides: { '=': 'none' } }],
        'padded-blocks': ['error', 'never'],
        // 'prefer-object-spread': 'error',
        'quote-props': ['error', 'consistent'],
        'quotes': ['error', 'single'],
        'semi': ['error', 'always'],
        'semi-spacing': ['error', { before: false, after: true }],
        'semi-style': ['error', 'last'],
        'space-before-blocks': ['error', 'always'],
        'space-before-function-paren': ['error', 'never'],
        'space-in-parens': ['error', 'never'],
        'space-infix-ops': 'error',
        'space-unary-ops': ['error', { 'words': true, 'nonwords': false }],
        'spaced-comment': ['error', 'always', { "block": { "exceptions": ["*"] } }],
        'switch-colon-spacing': ['error', { 'after': true, 'before': false }],
        'template-tag-spacing': ['error', 'never'],
        'unicode-bom': ['error', 'never'],

        // ECMAScript 6
        // https://eslint.org/docs/rules/#ecmascript-6
        // ----------------------------------------------
        'arrow-spacing': ['error', { "before": true, "after": true }],
        'generator-star-spacing': ['error', { before: false, after: true }],
        'no-confusing-arrow': ['error', { allowParens: true }],
        'no-duplicate-imports': ['error', { includeExports: false }],
        'no-useless-computed-key': 'error',
        'no-useless-rename': ['error', {
            'ignoreDestructuring': false,
            'ignoreImport': false,
            'ignoreExport': false
        }],
        // Eventually turn these on once we are fully converted to ES6
        'no-var': 'off',
        'object-shorthand': ['off', 'always', {
            'avoidQuotes': true,
            'ignoreConstructors': false
        }],
        'prefer-arrow-callback': ['off', {
            allowNamedFunctions: false,
            allowUnboundThis: true,
        }],
        'prefer-const': ['off', {
            'destructuring': 'all',
            'ignoreReadBeforeAssign': true,
        }],
        'prefer-numeric-literals': 'error',
        // Eventually turn these on once we get to es6
        'prefer-rest-params': 'off',
        'prefer-spread': 'off',
        'prefer-template': 'off',
        'rest-spread-spacing': ['error', 'never'],
        'symbol-description': 'error',
        'template-curly-spacing': ['error', 'never'],
        'yield-star-spacing': ['error', 'after'],
        // Custom rules
        'connect-internal/no-focused-tests': 'error'
    },
    'globals': {
        'define': false,
        'require': false,
        'module': false,
        'process': true
    },
    'overrides': [
        {
            'files': ['*Spec.js', '*.spec.js'],
            'globals': {
                '$': false,
                '_': false,
                'q': false,
                'EventEmitter': false,
                'inject': false,
                'angular': false,
                'jasmine': false,
                'beforeAll': false,
                'beforeEach': false,
                'afterEach': false,
                'spyOn': false,
                'spyOnEvent': false,
                'fail': false,
                'pass': false,
                'pending': false,
                'expect': false,
                'describe': false,
                'it': false,
                'xit': false,
                'xdescribe': false
            },
            'rules': {
                'no-new': 'off'
            }
        }
    ]
};
