let js2xmlparser = require('js2xmlparser');
let constants = {
    'InteractionType': 'ObjectRouting__InteractionType__c',
    'RemoteAddress': '0000000',
    'QueueName': 'ObjectRouting__QueueName__c',
    'RemoteName': 'ObjectRouting__SearchableID__c',
    'SenderAddress': 'ObjectRouting__SenderEmail__c',
    'RequestId': 'ObjectRouting__SearchableID__c',
    'Skills': 'ObjectRouting__Skills__c',
    'SF_URLPop': 'ObjectRouting__ItemId__c'
};

function updateKeys(data) {
    let modifiedData = {};
    for (let key in constants) {
        if (key === 'RemoteName') {
            modifiedData[key] = data[constants[key]] ? 'Case: ' + data[constants[key]] : '';
        } else {
            modifiedData[key] = data[constants[key]] || '';
        }
    }
    return modifiedData;
}

function convertJson2xml(data) {
    if (!data) { return; }
    let modifiedData = updateKeys(data);
    return js2xmlparser.parse('DataItem', modifiedData);
}

module.exports = convertJson2xml;
