const jsforce = require('jsforce');
const logger = require('../UTILS/trace')('SF-UTILS.sfStreaming');
const json2xml = require('./json2xml.js');
const icwsConnect = require('../IC-Common/icwsApiMethod');
var AsyncLock = require('async-lock');
let queueSubscriptionHelper = require('../IC-Common/queueSubscriptionHelper');
const credentials = require('../UTILS/credentials');
let backupDataUtil = require('../UTILS/recentQueueDetFileOps');

const conn = new jsforce.Connection();
const channel = '/topic/ININ_ObjectRouting';
var replayId = -1;
var subscription = null;
var lock = new AsyncLock();
let salesforceLoggedIn = false;

function isSalesforceLoggedIn() {
    return salesforceLoggedIn;
}

async function subscribeSfChannel() {
    logger.verbose('Subscribe to channel' + channel);
    try {
        var replayIdStr = await backupDataUtil.getRecentReplayId();
        replayId = Number(replayIdStr);
    } catch (error) {
        logger.error(error);
        replayId = -1;
    }
    
    logger.verbose(`Subscribe with Replay ID  ${replayId}`);
    const fayeClient = conn.streaming.createClient([new jsforce.StreamingExtension.Replay(channel, replayId)]);
    subscription = fayeClient.subscribe(channel, message => {
        logger.info(`Event Data: ${JSON.stringify(message.sobject)}`);
        logger.info(`replayId of the current object = ${message.event.replayId}`);
        logger.info(`Created date is : ${message.event.createdDate}`);
        if (!icwsConnect.isConnected()) {
            logger.warn('IC connection down, Stop processing Salesforce objects');
            unsubscribeSfChannel();
            logoutSf();
            return;
        }
        replayId = message.event.replayId;
        if (message.sobject.ObjectRouting__ObjectType__c === 'Case' && message.sobject.ObjectRouting__InteractionType__c === 'Email'
            && message.sobject.ObjectRouting__IsRoutingComplete__c === false) {
            if (message.sobject.ObjectRouting__Direction__c === 'Outgoing') {
                // Disconnect the interaction if auto disconnect is true
                if (credentials.icEnableAutoDisconnectOnReplay === 'on') {
                    icwsConnect.findAndDisconnectInteraction(message.sobject.ObjectRouting__ItemId__c);
                }
            } else {
                const xmlData = json2xml(message.sobject);
                logger.verbose(`Converted case data into xml Data as ${xmlData}`);
                logger.verbose('IC connection is live and sending dxml data');
                lock.acquire('SFKey', function(done) {
                    const workgroupName = message.sobject.ObjectRouting__QueueName__c;
                    const currentReplayId = message.event.replayId;
                    queueSubscriptionHelper.icwsQueueSubscription(workgroupName).then((status) => {
                        logger.info(`Status of Queue Subscription = ${status}`);
                        icwsConnect.icwsHandleNotification(xmlData, workgroupName, message.sobject.ObjectRouting__ItemId__c).then((status) => {
                            logger.info(`Status of handle notification = ${status}`);
                            done(status);
                        }).catch((err => {
                            if (err.message.includes('workgroup queue emails config')) {
                                logger.error(`Failed replay ID ${currentReplayId} for workgroup ${workgroupName}`);
                            }
                            logger.error(err.message);
                            done(err);
                        }));
                    }).catch((err) => {
                        logger.error(err.message);
                        done(err);
                    });
                }, function(state) {
                    logger.info(`Lock release here ${state}`);
                });
            }
            updateRoutingComplete(message.sobject.Id);
        }
    });
}

function updateRoutingComplete(objectId) {
    conn.sobject('ObjectRouting__ININ_Object_Routing_Queue__c').update({
        Id: objectId,
        // eslint-disable-next-line camelcase
        ObjectRouting__IsRoutingComplete__c: 'true'
    }, function(err, ret) {
        if (err || !ret.success) {
            return logger.error(err, ret);
        }
        logger.info('Updated Successfully : ' + ret.id);
    });
}

function unsubscribeSfChannel() {
    logger.verbose('Unsubscribe to channel ' + channel);
    if (subscription) {
        subscription.cancel();
        subscription = null;
    }
}

async function writeReplayIdToFile() {
    if (replayId === -1) { return Promise.resolve(); }
    let ret = await backupDataUtil.writeSfDataToFile('replayId', replayId);
    return ret;
}

function logoutSf() {
    conn.logout(function(err) {
        if (err !== undefined) {
            logger.error(err);
        }
        salesforceLoggedIn = false;
        logger.info('Salesforce session logged out');
    });
    setInterval(() => {
        if (conn && !conn.accessToken && icwsConnect.isConnected()) {
            logger.info('re-logging in to SalesForce');
            loginSf();
        }
    }, 3000 * 60);
}

function loginSf() {
    return conn.login(credentials.sfUserName, `${credentials.sfPwd}${credentials.sfToken}`, (err, res) => {
        if (err) { return logger.error(`unable to login to Salesforce ${err}`); }
        if (res) {
            logger.info('logged in successfully to Salesforce');
            salesforceLoggedIn = true;
            subscribeSfChannel();
        }
        return 'logged in successfully';
    });
}

module.exports = {isSalesforceLoggedIn, unsubscribeSfChannel, writeReplayIdToFile, loginSf};
