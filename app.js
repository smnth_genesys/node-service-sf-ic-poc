const path = require('path');
const express = require('express');
const bodyParser = require('body-parser');
let credentials = require('./UTILS/credentials');
const logger = require('./UTILS/trace')('app.js');
const sfUtils = require('./SF-UTILS/sfStreaming');
const icwsConnect = require('./IC-Common/icwsApiMethod');

const appVersion = require('./package.json').version;
const app = express();
const port = 5000;
const appName = 'Salesforce_IC_Integration';
const urlEncodedParser = bodyParser.urlencoded({ extended: false});
app.set('view engine', 'ejs');
// eslint-disable-next-line no-path-concat
// eslint-disable-next-line no-undef
app.use(express.static(path.join(__dirname, 'public')));

let icCred = {};

function connectCallback(success, result) {
    if (success) {
        logger.info('connection successful to IC server');
        if (this.cred) {
            icCred = this.cred;
        }
        if (this.res) {
            this.res.redirect('/loginSf');
            // Done with web page log in
            return;
        }
        // Re - login
        if (!sfUtils.isSalesforceLoggedIn()) {
            sfUtils.loginSf();
        }
    } else {
        logger.error('Failed to connect to IC server. Reason for failure: ' + result.responseText.message);
        if (this.res) {
            let responseText = result.responseText && result.responseText.message || 'The authentication process failed.';
            // eslint-disable-next-line no-undef
            this.res.render(path.join(__dirname, '/views/ic_cred.ejs'), {version: appVersion, statusMessage: responseText});
        }
    }
}

function disconnectCallBack(reason, shouldReconnect) {
    logger.verbose('IC Session is disconnected. Reason for disconnect is %s', reason);
    if (shouldReconnect) {
        logger.info(`IC Session Reconnecting to ${credentials.icServerName}`);
        Promise.all([sfUtils.writeReplayIdToFile(), icwsConnect.writeWorkgroupsToFile()]).then(() => {
            logger.info('Writes replay ID and workgroup lists successfully');
        }).catch((error) => {
            logger.error(`error thrown while writing the replay ID and workgroupLists ${error}`);
        });
        icwsConnect.icwsConnect(appName, credentials.icServerName, credentials.icUserName,
                                credentials.icPwd, connectCallback, disconnectCallBack);
    }
}

function setSfCredentials(cred) {
    credentials.sfToken = cred.sfToken;
    credentials.sfUserName = cred.sfUserName;
    credentials.sfPwd = cred.sfPwd;
}

app.get('/', (req, res) => {
    res.redirect('/loginIc');
});

app.get('/loginIc', (req, res) => {
    // eslint-disable-next-line no-undef
    res.render(path.join(__dirname, '/views/ic_cred.ejs'), {version: appVersion, statusMessage: undefined});
});

app.get('/loginSf', (req, res) => {
    // eslint-disable-next-line no-undef
    res.render(path.join(__dirname, '/views/sf_cred.ejs'), {version: appVersion, statusMessage: undefined});
});

app.get('/success', (req, res) => {
    // eslint-disable-next-line no-undef
    res.render(path.join(__dirname, '/views/success.ejs'), {version: appVersion});
});

app.post('/submitCred', urlEncodedParser, (req, res) => {
    let cred = req.body,
        bindObj = {
            res: res,
            cred: cred
        };
    logger.info(`Connecting to server: ${cred.icServerName}`);
    icwsConnect.icwsConnect(appName, cred.icServerName, cred.icUserName, cred.icPwd,
                            connectCallback.bind(bindObj), disconnectCallBack);
});

app.post('/submitSfCred', urlEncodedParser, (req, res) => {
    setSfCredentials(req.body);
    sfUtils.loginSf().then(() => {
        credentials.updateCredentialsFile({...icCred, ...req.body});
        logger.info('logged in succesfully into SF');
        res.redirect('/success');
    }).catch((e) => {
        logger.error(`Failed to log in to Salesforce ${e}`);
        // eslint-disable-next-line no-undef
        res.render(path.join(__dirname, '/views/sf_cred.ejs'), {version: appVersion, statusMessage: e.message});
    });
});

const server = app.listen(port, () => {
    logger.verbose('This app is listening on port %s!', port);
    if (!credentials.isCredentialsFileExists()) {
        return logger.verbose(`credentials file does not exist, please log in from http://localhost:${port}`);
    }
    logger.info('credentials file is exists, so trying to login to ic and sf using file credentials');
    credentials.defineCredentials().then(() => {
        // Login to ic and then to sf
        icwsConnect.icwsConnect(appName, credentials.icServerName, credentials.icUserName, credentials.icPwd,
                                connectCallback, disconnectCallBack);
    }).catch((e) => {
        logger.error(`something went wrong while defining and logging into the clients ${e}`);
    });
});

server.on('error', err => {
    // Handle specific listen errors with friendly messages
    switch (err.code) {
        case 'EACCES':
            logger.error('port requires elevated privileges');
            process.exit(1);
            break;
        case 'EADDRINUSE':
            logger.error('port is already in use');
            process.exit(1);
            break;
        default:
            logger.error('unable to create a server');
            throw err;
    }
});

/**
 *  Terminator === the termination handler
 *  Terminate server on receipt of the specified signal.
 *  @param {string} sig  Signal to terminate on.
 */
function terminator(sig) {
    if (typeof sig === 'string') {
        logger.info('%s: Received %s - terminating Salesforce Integration app ...', Date(Date.now()), sig);
        process.exit(1);
    }
    // Salesforce subscription cancel
    sfUtils.unsubscribeSfChannel();
    // Write last processed object replay ID and subscribed workgroups
    Promise.all([sfUtils.writeReplayIdToFile(), icwsConnect.writeWorkgroupsToFile(), icwsConnect.icwsDisconnect()]).then(() => {
        logger.info('%s: Node server stopped.', Date(Date.now()));
    }).catch((error) => {
        logger.error(`error thrown while terminating the app ${error}`);
    });
}

//  Setup termination handlers (for exit and a list of signals).
//  Process on exit and signals.
process.on('exit', function() { terminator(); });

// Handler execution for all these kill signals.
['SIGHUP',
    'SIGINT',
    'SIGQUIT',
    'SIGILL',
    'SIGTRAP',
    'SIGABRT',
    'SIGBUS',
    'SIGFPE',
    'SIGUSR1',
    'SIGSEGV',
    'SIGUSR2',
    'SIGTERM'].forEach(function(element) {
    process.on(element, function() { terminator(element); });
});

module.exports = server;
