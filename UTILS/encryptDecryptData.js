var crypto = require('crypto');
const logger = require('./trace')('encryptDecryptData.js');

const algorithm = 'aes-192-cbc',
    passwordKey = 'Password used to generate key',
    salt = 'Salt used for key',
    keyLength = 24,
    bufferSize = 16;

// Encrypt function
function encrypt(password) {
    return new Promise((resolve, reject) => {
        try {
            const key = crypto.scryptSync(passwordKey, salt, keyLength);

            // eslint-disable-next-line no-undef
            const iv = Buffer.alloc(bufferSize, 0);
            const cipher = crypto.createCipheriv(algorithm, key, iv);
            let encrypted = cipher.update(password, 'utf8', 'hex');
            encrypted += cipher.final('hex');

            return resolve(encrypted);
        } catch (error) {
            logger.error(`error occured in process of encrypting the data ${error}`);
            return reject(new Error(error));
        }
    });
}

// Decrypt function
function decrypt(encrypted) {
    return new Promise((resolve, reject) => {
        try {
            // Use the async `crypto.scrypt()` instead.
            const key = crypto.scryptSync(passwordKey, salt, keyLength);
            // The IV is usually passed along with the ciphertext.
            // eslint-disable-next-line no-undef
            const iv = Buffer.alloc(bufferSize, 0); // Initialization vector.

            const decipher = crypto.createDecipheriv(algorithm, key, iv);

            let decrypted = decipher.update(encrypted, 'hex', 'utf8');
            decrypted += decipher.final('utf8');
            return resolve(decrypted);
        } catch (error) {
            logger.error(`error occured in process of decrypting the data ${error}`);
            return reject(new Error(error));
        }
    });
}
    
module.exports = { encrypt, decrypt };
