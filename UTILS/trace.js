/*
    This is for logging the messages based on the levels

    Author: Sumanth

    Examples:

    1. logger.info('This is %s!', "Info");
    2. logger.error('This is %s!', "Error");
    3. logger.warn('This is %s!', "Warn");
    4. logger.verbose('This is %s!', "Vernose");
    5. logger.debug('This is %s!', "Debug");
    6. logger.silly('This is %s!', "Silly");

*/
const { createLogger, format, transports } = require('winston');
const {LOGS_BASE_PATH, NODE_ENV} = require('../constants.json');
require('winston-daily-rotate-file');

const { combine, colorize, splat, label, timestamp, printf } = format;

let fileTransport = new (transports.DailyRotateFile)({
    filename: 'application-%DATE%.log',
    datePattern: 'YYYY-MM-DD-HH',
    dirname: LOGS_BASE_PATH,
    zippedArchive: true,
    maxSize: '20m',
    maxFiles: '14d'
});

const logger = (topic) => {
    return createLogger({
        level: NODE_ENV === 'test' ? 'silent' : 'silly',
        format: combine(
            label({label: topic}),
            timestamp(),
            splat(),
            printf(info => `${info.timestamp} - ${info.label} - [${info.level}]: ${info.message}`)
        ),
        transports: [
            new transports.Console({
                format: combine(
                    label({label: topic}),
                    colorize(),
                    timestamp(),
                    splat(),
                    printf(info => `${info.timestamp} - ${info.label} - [${info.level}]: ${info.message}`)
                )
            }),
            fileTransport
        ]
    });
};

module.exports = logger;
