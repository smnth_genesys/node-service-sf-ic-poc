'use strict';

module.exports = function(context) {
    function check(node) {
        const name = node.callee.name;

        if (name === 'fdescribe' || name === 'fit') {
            context.report(node, 'Focused tests are not allowed.');
        }
    }

    return {
        CallExpression: check
    };
};
