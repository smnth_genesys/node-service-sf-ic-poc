const { writeToFile, readFromFile } = require('./fileOps');
const logger = require('./trace')('Utils-recentQueueDetFileOps.js');

const file = require('../constants.json').APP_EXIT_BACKUP_DATA_FILE;

async function writeSfDataToFile(dataType, writeData) {
    let ret = await writeToFile(file, dataType, writeData);
    return ret;
}
async function getRecentReplayId() {
    logger.info('getting recent replay ids');
    try {
        let fileData = await readFromFile(file);
        return fileData.split('\n').find(ele => ele.includes('replayId:')).replace('replayId:', '');
    } catch (error) {
        logger.warn('no replayId found');
        return -1;
    }
}

async function getRecentWorkgroups() {
    logger.info('getting recent workgroup ids');
    try {
        let fileData = await readFromFile(file);
        return fileData.split('\n').find(ele => ele.includes('workgroups:')).replace('workgroups:', '').split(',');
    } catch (error) {
        logger.error('something went wrong while extracting the workgroups');
        return [];
    }
}

module.exports = { writeSfDataToFile, getRecentReplayId, getRecentWorkgroups };
