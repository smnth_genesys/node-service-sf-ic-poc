const fs = require('fs');
const logger = require('./trace')('Utils-fileOps.js');
const path = require('path');

function isDirExists(file) {
    let dirName = path.dirname(file);
    return fs.existsSync(dirName) ? dirName : undefined;
}

function isFileExists(file) {
    return fs.existsSync(file);
}

function writeToFile(file, dataType, writeData) {
    return new Promise((resolve, reject) => {
        let updatedData;
        // Create new directory if directory doesn't exists
        if (!isDirExists(file)) {
            fs.mkdirSync(path.dirname(file), { recursive: true });
        }
        try {
            if (isFileExists(file)) {
                // If file exists, read data from file
                let readData = fs.readFileSync(file, {encoding: 'utf8'});
                if (readData.indexOf(dataType) !== -1) {
                    // Modify the data with value based on the dataType
                    let pattern = new RegExp('(' + dataType + ':)(.*)', 'i');
                    updatedData = readData.replace(pattern, `$1${writeData}`);
                } else {
                    /* Else: Assuming any of the following thing might happened:
                        1. No data type is added till now
                        2. Only one data type is added
                    */
                    updatedData = readData + dataType + ':' + writeData.toString();
                    updatedData += '\r\n';
                }
                fs.writeFileSync(file, updatedData, 'utf8');
                return resolve();
            }
            updatedData = dataType + ':' + writeData.toString();
            updatedData += '\r\n';
            fs.writeFileSync(file, updatedData, 'utf8');
            return resolve();
        } catch (error) {
            reject(new Error(`Failed while writing into file ${error}`));
        }
    });
}

function readFromFile(file) {
    return new Promise((resolve, reject) => {
        if (!isDirExists(file) && !isFileExists(file)) {
            logger.warn('directory or file not exists');
            return reject(new Error('directory or file not exists'));
        }

        fs.readFile(file, {encoding: 'utf8'}, (err, data) => {
            if (err || data.length === 0) {
                logger.error(`error found while reading the file, error: ${err} data: ${data} `);
                return reject(new Error(`Found error while reading the file ${err}`));
            }
            return resolve(data);
        });
    });
}

function writeJsonToFile(file, json) {
    return new Promise((resolve, reject) => {
        // Create new directory if directory doesn't exists
        if (!isDirExists(file)) {
            fs.mkdirSync(path.dirname(file), { recursive: true });
        }
        let stringfiedJson = JSON.stringify(json);
        fs.writeFileSync(file, stringfiedJson, (err) => {
            if (err) {
                logger.error(`Unable to update the file with the given json ${err}`);
                return reject(new Error(err));
            }
            logger.info('successfully updated the file with the json data');
            return resolve();
        });
    });
}

function readJsonFromFile(file) {
    return new Promise((resolve, reject) => {
        fs.readFile(file, (err, res) => {
            if (err || res.length === 0) {
                logger.error(`error found while reading the file, error: ${err} data: ${res} `);
                return reject(new Error(`Found error while reading the file ${err}`));
            }
            logger.info(`successfully read json from file ${res}`);
            return resolve(JSON.parse(res));
        });
    });
}

module.exports = { writeToFile, readFromFile, isDirExists, isFileExists, writeJsonToFile, readJsonFromFile };
