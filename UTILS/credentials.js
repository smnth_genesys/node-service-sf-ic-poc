/* Use Cases:

    Obj should be credentials = {ic: {}, sf: {}}
    1. For first time, agent logs in from the web page
        a. When agent clicks submit btn,
            i. should be able to save single data into file
            ii. should be able to save bulk of data into file ex: {icUserName: user1, icPwd: 1234, sfUserName: sdf}
            iii. use JSON.stringify on the bulk obj like shown above and savi stringified obj to the file.
            iv. easy to read as well using JSON.parse on the text read from the file.
        b. When one of the clients log out due to some reason
            i. check if credentialsData.data file exist
            ii. if not, log the error saying never logged in by the agent.
            iii. if file exist, should be able to fetch from credentials obj
                ex: this.ic.pwd, if this is undefined, then read all the value from file
    2. From next time,
        a. dfd
*/

const logger = require('./trace')('credentials.js');
let { encrypt, decrypt } = require('./encryptDecryptData');
let { isDirExists, isFileExists, writeJsonToFile, readJsonFromFile } = require('./fileOps');

const file = require('../constants.json').CREDENTIALS_FILE;

var credentials = {
    updated: false,
    isCredentialsFileExists() {
        return isDirExists(file) && isFileExists(file);
    },
    async assignCredentials(cred) {
        this.icUserName = cred.icUserName;
        this.icPwd = await decrypt(cred.icPwd);
        this.icServerName = cred.icServerName;
        this.sfUserName = cred.sfUserName;
        this.sfPwd = await decrypt(cred.sfPwd);
        this.sfToken = cred.sfToken;
        this.icEnableAutoDisconnectOnReplay = cred.icEnableAutoDisconnectOnReplay;
        this.updated = true;
    },
    async defineCredentials() {
        try {
            let cred = await readJsonFromFile(file);
            await this.assignCredentials(cred);
        } catch (error) {
            return logger.error(`unable to assign credentials due to error in the process of reading the file ${error}`);
        }
    },
    async updateCredentialsFile(cred) {
        if (cred.icPwd) {
            cred.icPwd = await encrypt(cred.icPwd);
        }
        if (cred.sfPwd) {
            cred.sfPwd = await encrypt(cred.sfPwd);
        }
        writeJsonToFile(file, cred);
    }
};

module.exports = credentials;
